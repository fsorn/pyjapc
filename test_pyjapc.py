# -*- coding: utf-8 -*-
"""
Created on Wed May 20 08:59:32 2015
 Testfile for pytest
 Start with py.test command
@author: mbetz

# see https://gitlab.cern.ch/acc-co/japc/japc-core/blob/develop/japc-ext-mockito2/src/java/cern/japc/ext/mockito/demo/Demo.java
"""
import jpype as jp
import time
import pytest
from unittest import mock as unittest_mock
from numpy import *
from pyjapc import *


japc = PyJapc(incaAcceleratorName=None)
cern = jp.JPackage('cern')
org = jp.JPackage('org')
acqVal = cern.japc.ext.mockito.JapcMock.acqVal
mockAllServices = cern.japc.ext.mockito.JapcMock.mockAllServices
mockParameter = cern.japc.ext.mockito.JapcMock.mockParameter
mpv = cern.japc.ext.mockito.JapcMock.mpv
pe = cern.japc.ext.mockito.JapcMock.pe
resetJapcMock = cern.japc.ext.mockito.JapcMock.resetJapcMock
resetToDefault = cern.japc.ext.mockito.JapcMock.resetToDefault
sel = cern.japc.ext.mockito.JapcMock.sel
whenGetValueThen = cern.japc.ext.mockito.JapcMock.whenGetValueThen
setGlobalAnswer = cern.japc.ext.mockito.JapcMock.setGlobalAnswer
spv = cern.japc.ext.mockito.JapcMock.spv
eq = org.mockito.ArgumentMatchers.eq
verify = org.mockito.Mockito.verify


def setup_module():
    mockAllServices()


def setup_function():
    # resetJapcMock breaks synchronization between Python and Java objects resulting in NullPointerException
    # resetToDefault on the other hand is not as desctructive and keeps working
    resetToDefault()


def teardown_function():
    japc.clearSubscriptions()


def convertHelper(dtype):
    """ Take a random value of type dtype,
        convert into Java World and back.
        Check if value changed
    """
    x = dtype(random.random() * 4200)
    y = japc._convertValToPy(japc._convertPyToVal(x))
    assert (x == y)


def test_convert_py_to_simple():
    assert japc._convertPyToSimpleVal(1).toString() == '(int:1) -> 1'
    assert japc._convertPyToSimpleVal(1.1).toString() == '(double:1) -> 1.1'
    assert japc._convertPyToSimpleVal(True).toString() == '(boolean:1) -> true'
    assert japc._convertPyToSimpleVal(False).toString() == '(boolean:1) -> false'
    assert japc._convertPyToSimpleVal("HelloTesti").toString() == '(String:1) -> HelloTesti'

    a = array(["hello", "world", "TestiUltraLongBlaString"])
    ja = japc._convertPyToSimpleVal(a)
    res = japc._convertSimpleValToPy(ja)
    assert res[0] == "hello"
    assert res[1] == "world"
    assert res[2] == "TestiUltraLongBlaString"
    assert res.size == 3
    sVal = japc._convertPyToVal({"a": 4, "b": ones((2, 2)), "c": "Wow", "d": True, "e": False})
    # assert( sVal.toString() == 'd (boolean:1) -> true\ne (boolean:1) -> false\nb (double[][]:2x2) -> [1.0, 1.0], [1.0, 1.0]\nc (String:1) -> Wow\na (int:1) -> 4\n' )
    # (Order is not always the same)
    a = ones(4, dtype=bool)
    a[2] = False
    assert japc._convertPyToSimpleVal(a).toString() == '(boolean[]:4) -> true, true, false, true'
    # Systematically check all Java types
    convertHelper(int16)
    convertHelper(int32)
    convertHelper(int64)
    convertHelper(float32)
    convertHelper(float64)
    convertHelper(double)
    convertHelper(bool)
    convertHelper(int)
    convertHelper(float)
    convertHelper(str)


def test_array_conversions():
    numpyArr = arange(4, dtype=double).reshape(2, 2)
    twoDParamValue = japc._convertPyToSimpleVal(numpyArr)
    # assert( type( twoDParamValue ) == jpype._jclass.cern.japc.spi.value.simple.DoubleArrayValue )
    reconvertedNumpyArr = japc._convertSimpleValToPy(twoDParamValue)
    assert array_equal(numpyArr, reconvertedNumpyArr)

    # Test an empty array (nasty!)
    numpyArr = arange(0, dtype=double)
    twoDParamValue = japc._convertPyToSimpleVal(numpyArr)
    # assert( type( twoDParamValue ) == jpype._jclass.cern.japc.spi.value.simple.DoubleArrayValue )
    reconvertedNumpyArr = japc._convertSimpleValToPy(twoDParamValue)
    assert array_equal(numpyArr, reconvertedNumpyArr)


def test_parameter_groups():
    p1 = "LHC.BQTrig.HB1/Acquisition#sequence"
    p2 = "LHC.BQTrig.HB2/Acquisition#sequence"
    gr = japc._getJapcPar([p1, p2])
    assert isinstance(gr, jp.JClass("cern.japc.core.spi.group.ParameterGroupImpl"))
    gr = japc._getJapcPar([p1])
    assert isinstance(gr, jp.JClass("cern.japc.core.spi.group.ParameterGroupImpl"))
    pa = japc._getJapcPar(p1)
    assert isinstance(pa, jp.JClass("cern.japc.core.spi.adaptation.FieldFilteringParameterAdapter"))


def test_selector_business():
    japc_obj = PyJapc(incaAcceleratorName=None)
    assert japc_obj._selector.toString() == "LHC.USER.ALL"
    assert japc_obj._selector.dataFilter is None
    japc_obj.setSelector("ALL", )
    assert japc_obj._selector.toString() == "ALL"
    assert japc_obj._selector.dataFilter is None
    japc_obj.setSelector("LHC.USER.ALL", {"averaging": 1})
    assert japc_obj._selector.toString() == "LHC.USER.ALL"
    assert japc_obj._selector.dataFilter.toString() == "averaging (int:1) -> 1\n"


def test_get_value():
    param = 'TEST/TestProperty'
    mock = mockParameter(param)
    whenGetValueThen(mock, sel('LHC.USER.TEST'), acqVal(param, 42, 0))
    japc.setSelector('LHC.USER.TEST')
    assert japc.getParam('TEST/TestProperty') == 42


def test_async_get():
    param = 'TEST/TestProperty'
    callback = unittest_mock.Mock()
    mock = mockParameter(param)
    whenGetValueThen(mock, sel('LHC.USER.TEST'), acqVal(param, 42, 0))
    japc.setSelector('LHC.USER.TEST')
    japc.getParam(parameterName='TEST/TestProperty', onValueReceived=callback)
    time.sleep(1)
    callback.assert_called_with(42)


def test_set_value():
    param = 'TEST/TestProperty'
    mock = mockParameter(param)
    japc.setSelector('LHC.USER.TEST')
    japc.setParam(param, 1.0, checkDims=False)
    verify(mock).setValue(sel('LHC.USER.TEST'), spv(1.0))


def test_multiple_selectors_same_no_param():
    param1 = 'TEST/TestProperty1'
    param2 = 'TEST/TestProperty2'
    mocj1 = mockParameter(param1)
    mocj2 = mockParameter(param2)
    sub1 = japc.subscribeParam(parameterName='TEST/TestProperty1', timingSelectorOverride='LHC.USER.TEST1')
    sub2 = japc.subscribeParam(parameterName='TEST/TestProperty1', timingSelectorOverride='LHC.USER.TEST2')
    sub3 = japc.subscribeParam(parameterName='TEST/TestProperty2', timingSelectorOverride='LHC.USER.TEST1')
    assert 'TEST/TestProperty1@LHC.USER.TEST1' in japc._subscriptionHandleDict
    assert 'TEST/TestProperty1@LHC.USER.TEST2' in japc._subscriptionHandleDict
    assert 'TEST/TestProperty2@LHC.USER.TEST1' in japc._subscriptionHandleDict
    assert not sub1.isMonitoring()
    assert not sub2.isMonitoring()
    assert not sub3.isMonitoring()
    japc.startSubscriptions()
    assert sub1.isMonitoring()
    assert sub2.isMonitoring()
    assert sub3.isMonitoring()
    japc.stopSubscriptions()
    assert not sub1.isMonitoring()
    assert not sub2.isMonitoring()
    assert not sub3.isMonitoring()


def test_multiple_selectors_same_no_selector_on_subscribe():
    param1 = 'TEST/TestProperty1'
    param2 = 'TEST/TestProperty2'
    mocj1 = mockParameter(param1)
    mocj2 = mockParameter(param2)
    japc.subscribeParam(parameterName='TEST/TestProperty1', timingSelectorOverride='LHC.USER.TEST1')
    japc.subscribeParam(parameterName='TEST/TestProperty1', timingSelectorOverride='LHC.USER.TEST2')
    japc.subscribeParam(parameterName='TEST/TestProperty2', timingSelectorOverride='LHC.USER.TEST1')
    assert 'TEST/TestProperty1@LHC.USER.TEST1' in japc._subscriptionHandleDict
    assert 'TEST/TestProperty1@LHC.USER.TEST2' in japc._subscriptionHandleDict
    assert 'TEST/TestProperty2@LHC.USER.TEST1' in japc._subscriptionHandleDict
    assert not japc._subscriptionHandleDict['TEST/TestProperty1@LHC.USER.TEST1'][0].isMonitoring()
    assert not japc._subscriptionHandleDict['TEST/TestProperty1@LHC.USER.TEST2'][0].isMonitoring()
    assert not japc._subscriptionHandleDict['TEST/TestProperty2@LHC.USER.TEST1'][0].isMonitoring()
    japc.startSubscriptions('TEST/TestProperty1')
    assert japc._subscriptionHandleDict['TEST/TestProperty1@LHC.USER.TEST1'][0].isMonitoring()
    assert japc._subscriptionHandleDict['TEST/TestProperty1@LHC.USER.TEST2'][0].isMonitoring()
    assert not japc._subscriptionHandleDict['TEST/TestProperty2@LHC.USER.TEST1'][0].isMonitoring()


def test_multiple_selectors_same_no_selector_on_unsubscribe():
    param1 = 'TEST/TestProperty1'
    param2 = 'TEST/TestProperty2'
    mocj1 = mockParameter(param1)
    mocj2 = mockParameter(param2)
    sub1 = japc.subscribeParam(parameterName='TEST/TestProperty1', timingSelectorOverride='LHC.USER.TEST1')
    sub2 = japc.subscribeParam(parameterName='TEST/TestProperty1', timingSelectorOverride='LHC.USER.TEST2')
    sub3 = japc.subscribeParam(parameterName='TEST/TestProperty2', timingSelectorOverride='LHC.USER.TEST1')
    assert not sub1.isMonitoring()
    assert not sub2.isMonitoring()
    assert not sub3.isMonitoring()
    japc.startSubscriptions()
    assert sub1.isMonitoring()
    assert sub2.isMonitoring()
    assert sub3.isMonitoring()
    japc.stopSubscriptions('TEST/TestProperty1')
    assert not sub1.isMonitoring()
    assert not sub2.isMonitoring()
    assert sub3.isMonitoring()


def test_multiple_selectors_same_with_selector_on_subscribe():
    param1 = 'TEST/TestProperty1'
    param2 = 'TEST/TestProperty2'
    mocj1 = mockParameter(param1)
    mocj2 = mockParameter(param2)
    sub1 = japc.subscribeParam(parameterName='TEST/TestProperty1', timingSelectorOverride='LHC.USER.TEST1')
    sub2 = japc.subscribeParam(parameterName='TEST/TestProperty1', timingSelectorOverride='LHC.USER.TEST2')
    sub3 = japc.subscribeParam(parameterName='TEST/TestProperty2', timingSelectorOverride='LHC.USER.TEST1')
    assert not sub1.isMonitoring()
    assert not sub2.isMonitoring()
    assert not sub3.isMonitoring()
    japc.startSubscriptions(parameterName='TEST/TestProperty1', selector='LHC.USER.TEST1')
    assert sub1.isMonitoring()
    assert not sub2.isMonitoring()
    assert not sub3.isMonitoring()


def test_multiple_selectors_same_with_selector_on_unsubscribe():
    param1 = 'TEST/TestProperty1'
    param2 = 'TEST/TestProperty2'
    mocj1 = mockParameter(param1)
    mocj2 = mockParameter(param2)
    sub1 = japc.subscribeParam(parameterName='TEST/TestProperty1', timingSelectorOverride='LHC.USER.TEST1')
    sub2 = japc.subscribeParam(parameterName='TEST/TestProperty1', timingSelectorOverride='LHC.USER.TEST2')
    sub3 = japc.subscribeParam(parameterName='TEST/TestProperty2', timingSelectorOverride='LHC.USER.TEST1')
    assert 'TEST/TestProperty1@LHC.USER.TEST1' in japc._subscriptionHandleDict
    assert 'TEST/TestProperty1@LHC.USER.TEST2' in japc._subscriptionHandleDict
    assert 'TEST/TestProperty2@LHC.USER.TEST1' in japc._subscriptionHandleDict
    assert not sub1.isMonitoring()
    assert not sub2.isMonitoring()
    assert not sub3.isMonitoring()
    japc.startSubscriptions()
    assert sub1.isMonitoring()
    assert sub2.isMonitoring()
    assert sub3.isMonitoring()
    japc.stopSubscriptions(parameterName='TEST/TestProperty1', selector='LHC.USER.TEST1')
    assert not sub1.isMonitoring()
    assert sub2.isMonitoring()
    assert sub3.isMonitoring()


def test_subscribe_specific_no_selectors():
    param1 = 'TEST/TestProperty1'
    param2 = 'TEST/TestProperty2'
    mocj1 = mockParameter(param1)
    mocj2 = mockParameter(param2)
    japc.setSelector('LHC.USER.TEST')
    sub1 = japc.subscribeParam('TEST/TestProperty1')
    sub2 = japc.subscribeParam('TEST/TestProperty2')
    assert 'TEST/TestProperty1' in japc._subscriptionHandleDict
    assert 'TEST/TestProperty2' in japc._subscriptionHandleDict
    assert not sub1.isMonitoring()
    assert not sub2.isMonitoring()
    japc.startSubscriptions('TEST/TestProperty1')
    assert sub1.isMonitoring()
    assert not sub2.isMonitoring()


def test_subscribe_all_no_selectors():
    param1 = 'TEST/TestProperty1'
    param2 = 'TEST/TestProperty2'
    mocj1 = mockParameter(param1)
    mocj2 = mockParameter(param2)
    japc.setSelector('LHC.USER.TEST')
    sub1 = japc.subscribeParam('TEST/TestProperty1')
    sub2 = japc.subscribeParam('TEST/TestProperty2')
    assert 'TEST/TestProperty1' in japc._subscriptionHandleDict
    assert 'TEST/TestProperty2' in japc._subscriptionHandleDict
    assert not sub1.isMonitoring()
    assert not sub2.isMonitoring()
    japc.startSubscriptions()
    assert sub1.isMonitoring()
    assert sub2.isMonitoring()


def test_unsubscribe_specific_no_selectors():
    param1 = 'TEST/TestProperty1'
    param2 = 'TEST/TestProperty2'
    mocj1 = mockParameter(param1)
    mocj2 = mockParameter(param2)
    japc.setSelector('LHC.USER.TEST')
    sub1 = japc.subscribeParam('TEST/TestProperty1')
    sub2 = japc.subscribeParam('TEST/TestProperty2')
    japc.startSubscriptions()
    assert 'TEST/TestProperty1' in japc._subscriptionHandleDict
    assert 'TEST/TestProperty2' in japc._subscriptionHandleDict
    assert sub1.isMonitoring()
    assert sub2.isMonitoring()
    japc.stopSubscriptions('TEST/TestProperty1')
    assert 'TEST/TestProperty1' in japc._subscriptionHandleDict
    assert 'TEST/TestProperty2' in japc._subscriptionHandleDict
    assert not sub1.isMonitoring()
    assert sub2.isMonitoring()


def test_unsubscribe_all_no_selectors():
    param1 = 'TEST/TestProperty1'
    param2 = 'TEST/TestProperty2'
    mocj1 = mockParameter(param1)
    mocj2 = mockParameter(param2)
    japc.setSelector('LHC.USER.TEST')
    sub1 = japc.subscribeParam('TEST/TestProperty1')
    sub2 = japc.subscribeParam('TEST/TestProperty2')
    japc.startSubscriptions()
    assert 'TEST/TestProperty1' in japc._subscriptionHandleDict
    assert 'TEST/TestProperty2' in japc._subscriptionHandleDict
    assert sub1.isMonitoring()
    assert sub2.isMonitoring()
    japc.stopSubscriptions()
    assert 'TEST/TestProperty1' in japc._subscriptionHandleDict
    assert 'TEST/TestProperty2' in japc._subscriptionHandleDict
    assert not sub1.isMonitoring()
    assert not sub2.isMonitoring()


def test_subscribe_multiple_subscriptions_same_param_no_selectors():
    param1 = 'TEST/TestProperty1'
    mocj1 = mockParameter(param1)
    def handler1(p, v):
        pass
    def handler2(p, v):
        pass
    japc.setSelector('LHC.USER.TEST')
    sub1 = japc.subscribeParam('TEST/TestProperty1', handler1)
    sub2 = japc.subscribeParam('TEST/TestProperty1', handler2)
    assert 'TEST/TestProperty1' in japc._subscriptionHandleDict
    assert not sub1.isMonitoring()
    assert not sub2.isMonitoring()
    japc.startSubscriptions('TEST/TestProperty1')
    assert sub1.isMonitoring()
    assert sub2.isMonitoring()
    japc.stopSubscriptions('TEST/TestProperty1')
    assert not sub1.isMonitoring()
    assert not sub2.isMonitoring()
