PyJapc
======

PyJapc is a Python to FESA/LSA/INCA interface via JAPC.

Full documentation and examples are available on the Scripting Tools wiki:

https://wikis.cern.ch/display/ST/PyJapc


Testing
=======

To run test, install required libraries:

pip install .[testing]

Then run tests

pytest .
